<%-- 
    Document   : HelloJsp
    Created on : Nov 24, 2015, 11:23:53 AM
    Author     : Michiel Noback [m.a.noback@pl.hanze.nl]
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        how nice to see you.
        <h3>Current time is ${requestScope.date}</h3>
        <h3>${requestScope.message}</h3>
    </body>
</html>
