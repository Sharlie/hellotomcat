<%-- 
    Document   : login_form
    Created on : Nov 30, 2015, 1:33:01 PM
    Author     : Michiel Noback [m.a.noback@pl.hanze.nl]
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action = "<c:url value="/login.do"/>" method = "POST">
    <table>
        <tr>
            <td>Your user name</td><td><input type="text" name = "username" required></td>
        </tr>
        <tr>
            <td>Your password</td><td><input type = "password" name = "password" required/></td>
        </tr>
        <tr>
            <td></td><td><input type="submit" value = "ENTER"></td>
        </tr>
    </table>
</form>
