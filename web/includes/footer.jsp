<%-- 
    Document   : footer
    Created on : Nov 30, 2015, 1:55:27 PM
    Author     : Michiel Noback [m.a.noback@pl.hanze.nl]
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<p>
&copy; 2015 Michiel Noback (${initParam.admin_email}) All rights reserved
</p>
