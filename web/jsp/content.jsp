<%-- 
    Document   : content
    Created on : Nov 26, 2015, 11:36:10 AM
    Author     : Michiel Noback [m.a.noback@pl.hanze.nl]
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <c:choose>
            <c:when test = "${sessionScope.user != null}">
                <c:choose>
                    <c:when test = "${sessionScope.user.name == 'Michiel'}">
                        <c:redirect url="http://www.waarneming.nl"/>
                    </c:when>
                    <c:otherwise>
                        <h1>Hello ${sessionScope.user.userMessage}</h1>   
                        Here you will find our amazingly secret data.
                        
                        <table>
                            <thead>
                                <tr>
                                    <td>
                                        Author
                                    </td>
                                    <td>
                                        Books
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var = "author" items="${requestScope.authors}">
                                    <tr>
                                        <td>
                                            ${author.name}
                                        </td>
                                        <td>
                                            <c:forEach var = "book" items="${author.books}" varStatus="count">
                                                title ${count.index + 1}: ${book.title}, ${book.pages} pages<br />
                                            </c:forEach>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </c:otherwise>
                </c:choose>
            </c:when>
            <c:otherwise>
                <h3>You are not logged in to our site! Please do so first </h3>
                <jsp:include page="/includes/login_form.jsp" />
            </c:otherwise>
        </c:choose>
        
        <jsp:include page="/includes/footer.jsp" />
        
    </body>
</html>
