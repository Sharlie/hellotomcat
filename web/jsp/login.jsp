<%-- 
    Document   : login.jsp
    Created on : Nov 26, 2015, 10:51:56 AM
    Author     : Michiel Noback [m.a.noback@pl.hanze.nl]
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>My super secret page</title>
    </head>
    <body>
        <h1>Content of corporate site is available only to logged in users</h1>
        <h3>${requestScope.errorMessage}</h3>
        <jsp:include page="/includes/login_form.jsp" />
        <jsp:include page="/includes/footer.jsp" />
    </body>
</html>
