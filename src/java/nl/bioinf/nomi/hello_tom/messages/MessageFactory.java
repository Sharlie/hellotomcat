/*
 * Copyright (c) 2015 Michiel Noback [m.a.noback@pl.hanze.nl]
 * All rights reserved.
 */
package nl.bioinf.nomi.hello_tom.messages;

import java.text.DateFormatSymbols;
import java.util.Calendar;

/**
 *
 * @author Michiel Noback [m.a.noback@pl.hanze.nl]
 * @version 0.0.1
 */
public class MessageFactory {
    public static String getMessage(){
        String[] dayNames = new DateFormatSymbols().getWeekdays();
        Calendar date = Calendar.getInstance();
        String message = "Today is a "
                + dayNames[date.get(Calendar.DAY_OF_WEEK)]
                + "; ";
        if (date.get(Calendar.DAY_OF_WEEK) == 1) {
            message += "you should not work sundays!";
        } 
        else if (date.get(Calendar.DAY_OF_WEEK) == 7) {
            message += "is is saturday, time to rest!";
        } else {
            message += "working like a zombie..";
        }
        return message;
    }
}