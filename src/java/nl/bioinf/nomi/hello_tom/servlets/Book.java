/*
 * Copyright (c) 2015 Michiel Noback [m.a.noback@pl.hanze.nl]
 * All rights reserved.
 */
package nl.bioinf.nomi.hello_tom.servlets;
/**
 *
 * @author Michiel Noback [m.a.noback@pl.hanze.nl]
 * @version 0.0.1
 */
public class Book {
    /**
     * the book title.
     */
    private String title;
    /**
     * the book author.
     */
    private Author author;
    /**
     * the number of pages.
     */
    private int pages;

    /**
     * default constructor.
     */
    public Book() { }

    /**
     * utility constructor.
     * @param title the book title
     * @param author the author
     * @param pages the number of pages
     */
    public Book(final String title, final Author author, final int pages) {
        this.title = title;
        this.author = author;
        this.pages = pages;
    }

    /**
     *
     * @return title
     */
    public String getTitle() {
        return title;
    }

    /**
     *
     * @param title the book title
     */
    public void setTitle(final String title) {
        this.title = title;
    }

    /**
     *
     * @return author
     */
    public Author getAuthor() {
        return author;
    }

    /**
     *
     * @param author the book author
     */
    public void setAuthor(final Author author) {
        this.author = author;
    }

    /**
     *
     * @return pages
     */
    public int getPages() {
        return pages;
    }

    /**
     *
     * @param pages the number of pages
     */
    public void setPages(final int pages) {
        this.pages = pages;
    }

    @Override
    public String toString() {
        return "Book{" + "title=" + title + ", author=" + author + ", pages=" + pages + '}';
    }
}
