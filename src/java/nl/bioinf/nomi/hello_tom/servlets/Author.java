/*
 * Copyright (c) 2015 Michiel Noback [m.a.noback@pl.hanze.nl]
 * All rights reserved.
 */
package nl.bioinf.nomi.hello_tom.servlets;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Michiel Noback [m.a.noback@pl.hanze.nl]
 * @version 0.0.1
 */
class Author {
    /**
     * the authors first name.
     */
    private String firstName;
    /**
     * the authors last name.
     */
    private String lastName;
    /**
     * the authors infix.
     */
    private String infix;
    /**
     * the authors books.
     */
    private List<Book> books;

    /**
     * bean constructor.
     */
    public Author() {
        init();
    }

    /**
     * full constructor for author name.
     * @param firstName the first name
     * @param lastName the last name
     * @param infix the infix
     */
    public Author(final String firstName, final String lastName, final String infix) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.infix = infix;
        init();
    }

    /**
     * full constructor for author name and books.
     * @param firstName the first name
     * @param lastName the last name
     * @param infix the infix
     * @param books the book list for this author
     */
    public Author(final String firstName, final String lastName, final String infix, final List<Book> books) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.infix = infix;
        this.books = books;
        init();
    }

    /**
     * INIT CODE.
     */
    private void init() {
        if (books == null) {
            books = new ArrayList<>();
        }
    }

    /**
     *
     * @return firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     *
     * @param firstName the first name
     */
    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    /**
     *
     * @return lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     *
     * @param lastName the last name
     */
    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    /**
     *
     * @return infix
     */
    public String getInfix() {
        return infix;
    }

    /**
     *
     * @param infix the name infix
     */
    public void setInfix(final String infix) {
        this.infix = infix;
    }

    /**
     * returns the full name constructed from firstname, lastname and infix (if present).
     * @return name
     */
    public String getName() {
        StringBuilder sb = new StringBuilder(firstName);
        if (infix != null) {
            sb.append(" ");
            sb.append(infix);
        }
        sb.append(" ");
        sb.append(lastName);
        return sb.toString();
    }

    /**
     *
     * @return books
     */
    public List<Book> getBooks() {
        return books;
    }

    /**
     * sets the books of this author, replacing a pre-existing one of present.
     * @param books the books to set
     */
    public void setBooks(final List<Book> books) {
        this.books = books;
    }

    /**
     * adds a book to this author.
     * @param newBook the new book
     */
    public void addBook(final Book newBook) {
        this.books.add(newBook);
    }

    @Override
    public String toString() {
        int numBooks = this.books == null ? 0 : this.books.size();
        return this.getClass().getSimpleName()
                + "[name=" + getName()
                + "; No books=" + numBooks + "]";
    }
}